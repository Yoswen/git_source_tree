import pyautogui
import time

class Cordinates():
    start_game = (1036,845)
    quick_match = (1032,639)
    buy_dice = (930,865)
    end_game = (930,865)
    play_add = (1137,841)
    cancel_exit = (1063,613)
    whats_app_close = (1110,577)
    do_not_reset = (1133,406)
    button_card_box = (1046,203)
    open_card_box = (930,780)
    shake_card_box = (925,520)
    open_at_once = (928,760)
    end_card_box = (931,807)

class Variables():
    nombre_tickets = 0
    nombre_boites = 0
    nombre_crédits = 0

def openCardBox():
    print('Combien avez-vous de ticket en tout?')
    Variables.nombre_tickets = int(input())
    Variables.nombre_boites = Variables.nombre_tickets / 40
    print('Vous avez donc '+ str(Variables.nombre_boites))
    print('boites à ouvrir')
    while (Variables.nombre_tickets > 80):
        pyautogui.click(Cordinates.button_card_box)
        time.sleep(1)
        pyautogui.click(Cordinates.open_card_box)
        time.sleep(1)
        pyautogui.click(Cordinates.shake_card_box)
        time.sleep(5)
        pyautogui.click(Cordinates.open_at_once)
        time.sleep(1)
        pyautogui.click(Cordinates.end_card_box)
        Variables.nombre_boites = Variables.nombre_boites - 1
        Variables.nombre_tickets = Variables.nombre_tickets - 40
        print("Reste à ouvrir  %s tickets" %Variables.nombre_tickets)
        print("Ce qui fait %s boites" %Variables.nombre_boites)

def pressEscape():
    pyautogui.keyDown('escape')
    time.sleep(0.05)
    pyautogui.keyUp('escape')
    time.sleep(1)

def start_game():
    pyautogui.click(Cordinates.start_game)
    time.sleep(1)
    pyautogui.click(Cordinates.quick_match)
    pyautogui.click(Cordinates.do_not_reset) #si pas de crédit et clic sur start coop on annule l'achat en gemmes

def play_coop_mode():
    time.sleep(8) #waiting for game room to load
    pyautogui.click(Cordinates.buy_dice)
    pyautogui.click(Cordinates.buy_dice)
    pyautogui.click(Cordinates.buy_dice)
    pyautogui.click(Cordinates.buy_dice)
    pyautogui.click(Cordinates.buy_dice)
    pyautogui.click(Cordinates.buy_dice)
    pyautogui.click(Cordinates.buy_dice)
    pyautogui.click(Cordinates.buy_dice)
    pyautogui.click(Cordinates.end_game)
    time.sleep(5) #waiting for game HomePage to load if game ended
    pyautogui.click(Cordinates.buy_dice)
    pyautogui.click(Cordinates.buy_dice)
    pyautogui.click(Cordinates.buy_dice)
    pyautogui.click(Cordinates.end_game)
    time.sleep(7)

def watch_add():
    pyautogui.click(Cordinates.play_add)
    time.sleep(35)
    pressEscape()
    pyautogui.click(Cordinates.whats_app_close)
    pyautogui.click(Cordinates.cancel_exit)
    time.sleep(2)

def launchBot():
    while True:
        start_game()
        play_coop_mode()
        watch_add()

# décommenter les lignes ci-dessous pour exécuter le code :
#openCardBox()
launchBot()